FROM python:3.10-slim-buster

WORKDIR /app

RUN apt-get update && apt-get install -y gcc python3-dev
RUN pip install uv

COPY . .

ENV VIRTUAL_ENV=/usr/local
RUN uv pip compile pyproject.toml -o requirements.txt && uv pip install -r requirements.txt

ARG ENV=prod
RUN if [ "$ENV" = "dev" ]; then \
        uv pip install -r requirements_dev.txt; \
    fi