SCALERS = ["standard", "robust"]
MODELS = ["forest", "boosting"]

rule all:
    input:
        expand('output/model_{model}_{scaler}.pickle', scaler=SCALERS, model=MODELS)
    run:
        shell("lakectl local commit output -m 'Add output files'")

rule get:
    output: 'output/data_raw.pickle'
    shell:
        'python src/data/extract_dataset.py'

rule preprocess:
    input: 'output/data_raw.pickle'
    output: 'output/data_preprocessed_{scaler}.pickle'
    run:
        shell("python src/data/preprocess_dataset.py --scaler {wildcards.scaler}")
        shell("lakectl fs upload lakefs://ods-mlops/main/data_preprocessed_{wildcards.scaler}.pickle -s output/data_preprocessed_{wildcards.scaler}.pickle")


rule train:
    input: 'output/data_preprocessed_{scaler}.pickle'
    output: 'output/model_{model}_{scaler}.pickle'
    threads: int(os.getenv("CPU_COUNT")) // 2
    run:
        shell("lakectl fs download lakefs://ods-mlops/main/data_preprocessed_{wildcards.scaler}.pickle output/data_preprocessed_{wildcards.scaler}.pickle")
        shell("""python src/pipelines/main.py \
            --model_type {wildcards.model} \
            --input_filename data_preprocessed_{wildcards.scaler}.pickle""")