import os
from dataclasses import dataclass, field


@dataclass
class Preprocessing:
    copy: bool = True


@dataclass
class StandardPreprocessing(Preprocessing):
    with_mean: bool = True
    with_std: bool = True


@dataclass
class RobustPreprocessing(Preprocessing):
    with_centering: bool = True
    with_scaling: bool = True


@dataclass
class PreprocessingConfig:
    standard: StandardPreprocessing = field(default_factory=StandardPreprocessing)
    robust: RobustPreprocessing = field(default_factory=RobustPreprocessing)


@dataclass
class Training:
    random_state: int = 42


@dataclass
class ForestTraining(Training):
    _target_: str = "sklearn.ensemble.RandomForestRegressor"
    n_estimators: int = 100
    criterion: str = "squared_error"
    max_depth: int = 20
    n_jobs: int = int(os.getenv("CPU_COUNT")) // 2


@dataclass
class BoostingTraining:
    _target_: str = "sklearn.ensemble.HistGradientBoostingRegressor"
    loss: str = "squared_error"
    learning_rate: float = 3e-4
    max_depth: int = 5


@dataclass
class TrainingConfig:
    forest: ForestTraining = field(default_factory=ForestTraining)
    boosting: BoostingTraining = field(default_factory=BoostingTraining)
