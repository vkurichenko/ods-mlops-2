"""
Main pipeline
"""

import os
import pickle
import sys

import click
import pandas as pd
from hydra import compose, initialize
from hydra.core.config_store import ConfigStore
from hydra.utils import instantiate

PROJECT_DIR = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)

sys.path.append(PROJECT_DIR)

from src.conf.config import TrainingConfig

cs = ConfigStore.instance()
cs.store(name="config", node=TrainingConfig)


@click.command()
@click.option("--model_type", default="boosting", help="Model to use.")
@click.option("--input_filename", help="Input path.")
# @click.option("--n_jobs", default=-1, help="Multiprocessing.")
def main(model_type, input_filename):
    """Train model."""

    initialize(version_base=None, config_path=".")
    cfg = compose(config_name="config")

    data = pd.read_pickle(os.path.join(PROJECT_DIR, "output", input_filename))
    X = data.drop(columns=["MedHouseVal"])
    y = data["MedHouseVal"]

    model = instantiate(cfg[model_type])
    model.fit(X, y)

    with open(
        os.path.join(
            PROJECT_DIR,
            "output",
            f"model_{model_type}_{input_filename.split('_')[-1].strip('.pickle')}.pickle",
        ),
        "wb",
    ) as file:
        pickle.dump(model, file)


if __name__ == "__main__":
    main()
