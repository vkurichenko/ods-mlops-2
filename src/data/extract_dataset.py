"""
Dataset extraction from Materials Project
"""

import os

import pandas as pd
from sklearn import datasets

PROJECT_DIR = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)


def extract_dataset() -> None:
    """
    Extract sklearn dataset.
    """
    X, y = datasets.fetch_california_housing(as_frame=True, return_X_y=True)
    dataset = pd.concat([X, y], axis=1)
    dataset.to_pickle(os.path.join(PROJECT_DIR, "output", "data_raw.pickle"))


if __name__ == "__main__":
    extract_dataset()
