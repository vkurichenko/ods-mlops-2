"""
Dataset preprocess
"""

import os
import sys

import click
import pandas as pd
from hydra import compose, initialize
from hydra.core.config_store import ConfigStore
from sklearn.preprocessing import RobustScaler, StandardScaler

PROJECT_DIR = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)
sys.path.append(PROJECT_DIR)

from src.conf.config import PreprocessingConfig

cs = ConfigStore.instance()
cs.store(name="config", node=PreprocessingConfig)


@click.command()
@click.option("--scaler", default="standard", help="Scaler to use.")
def extract_dataset(scaler: str) -> None:
    """
    Preprocess sklearn dataset.
    """

    initialize(version_base=None, config_path=".")
    cfg = compose(config_name="config")

    scaler_map = {"standard": StandardScaler, "robust": RobustScaler}

    data = pd.read_pickle(os.path.join(PROJECT_DIR, "output", "data_raw.pickle"))
    X = data.drop(columns=["MedHouseVal"])
    y = data["MedHouseVal"]

    sc = scaler_map[scaler](**cfg[scaler])
    X_sc = pd.DataFrame(sc.fit_transform(X), columns=sc.feature_names_in_)
    dataset = pd.concat([X_sc, y], axis=1)

    dataset.to_pickle(os.path.join(PROJECT_DIR, "output", f"data_preprocessed_{scaler}.pickle"))


if __name__ == "__main__":
    extract_dataset()
