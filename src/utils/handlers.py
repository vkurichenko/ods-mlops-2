"""
Utils
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.io.cif import CifParser
from sklearn.metrics import mean_absolute_error, r2_score

periodic_table = pd.read_csv(
    "/Users/vkurichenko/Downloads/ods-mlops/data/external/periodic_table.csv"
)
valency = pd.read_csv(
    "/Users/vkurichenko/Downloads/ods-mlops/data/external/default_paw_potentials_vasp.csv",
    sep=";",
)
electronegativity = pd.read_csv(
    "/Users/vkurichenko/Downloads/ods-mlops/data/external/pauling_scale.csv", sep=";"
)


def m_n_density_contribute(x):
    """Get density contribution for m_n"""

    if 0 < x < 3.4e-8:
        x = "high"
    elif 3.4e-8 < x < 0.7:
        x = "medium"
    else:
        x = "low"
    return x


def m_p_density_contribute(x):
    """Get density contribution for m_p"""

    if 0 < x < 1e-7:
        x = "high"
    elif 1e-7 < x < 0.7:
        x = "medium"
    else:
        x = "low"
    return x


def get_struct(cif_string):
    """Get structure"""

    cif_parsed = CifParser.from_string(cif_string)
    struct = cif_parsed.get_structures()[0]
    return struct


def get_space_group(cif_string: str) -> str:
    """Get space group from CIF file"""

    parsed_cif = CifParser.from_string(cif_string)
    structure = parsed_cif.get_structures()[0]
    return structure.get_space_group_info()[0]


def get_ase_structure(cif_string: str) -> str:
    """ASE structure calculation function"""

    parsed_cif = CifParser.from_string(cif_string)
    structure = parsed_cif.get_structures()[0]
    return AseAtomsAdaptor.get_atoms(structure)


def get_VEC(unit_cell_formula):
    """VEC calculation function"""

    S = sum(unit_cell_formula.values())
    valency_sum = 0
    for elem in unit_cell_formula.keys():
        V = valency[valency["Element"] == elem].valency.values[0]
        N = unit_cell_formula.get(elem)
        valency_sum += V * N
    VEC = valency_sum / S
    return VEC


def get_TEN(unit_cell_formula):
    """TEN calculation function"""

    S = sum(unit_cell_formula.values())
    ten_sum = 0
    for elem in unit_cell_formula.keys():
        if electronegativity[electronegativity["Symbol"] == elem].use.values[0] == "no data":
            return np.nan
        else:
            ten = float(electronegativity[electronegativity["Symbol"] == elem].use.values)
            N = unit_cell_formula.get(elem)
            ten_sum += ten * N
            TEN = ten_sum / S
            return TEN


def get_weights(unit_cell_formula):
    """Get weights"""

    masses = {}

    for item in unit_cell_formula:
        atomic_mass = periodic_table[periodic_table["Symbol"] == item]["AtomicMass"].values
        atomic_concentration = unit_cell_formula.get(item)
        masses[item] = float(atomic_mass) * float(atomic_concentration)

    weight_percents = {}

    for item in masses.items():
        weight_percents[item[0]] = round(item[1] / sum(masses.values()) * 100, 2)

    return weight_percents


def get_concentrations(unit_cell_formula):
    """Get concentrations"""

    natoms = sum(list(unit_cell_formula.values()))
    atomic_percents = {}
    for item in unit_cell_formula.items():
        atomic_percents[item[0]] = round(item[1] / natoms, 2)
    return atomic_percents


def plot_corr(df):
    """Plot correlation"""

    corr_matrix = df.corr()
    plt.figure(figsize=(6, 6))
    plt.imshow(corr_matrix, cmap="coolwarm", vmin=-1.0, vmax=1)
    plt.colorbar()  # add color intensity map
    plt.xticks(range(len(corr_matrix.columns)), corr_matrix.columns, rotation=90)
    plt.yticks(range(len(corr_matrix)), corr_matrix.index)
    plt.show()


def plot_true_vs_pred(y_true, y_pred):
    """Plot true vs pred"""

    fig, ax = plt.subplots(figsize=(6, 6))
    r2 = round(r2_score(y_true, y_pred), 3)
    mae = round(mean_absolute_error(y_true, y_pred), 3)
    std_pred = round(y_pred.std())
    std_true = round(y_true.std())  # .values[0])
    text = [
        f"$R^2_{{test}}$={r2}",
        f"MAE={mae}",
        f"$\sigma_{{true}}$={std_true}",
        f"$\sigma_{{pred}}$={std_pred}",
    ]
    text = "\n".join(text)
    ax.plot(y_true, y_pred, "o", label=text)
    ax.set_xlim(y_pred.min(), y_pred.max())
    ax.set_ylim(y_pred.min(), y_pred.max())
    ax.plot([-1, 1], [-1, 1], transform=ax.transAxes, c="grey", ls="--")
    ax.legend()
    ax.set_xlabel("True values")
    ax.set_ylabel("Predicted values")
    plt.show()
