Прогнозирование материалов с большим магнитно‑сопротивлением
==============================

В данном репозитории хранится код проекта, выполняемого в рамках диссертационной работы «Магнитные, тепло и электрофизические свойства материалов на основе халькогенидов металлов состава (Pb, Bi)SnS<sub>2</sub>​, Cu(CrTi)S<sub>4</sub>».

Проект ведется по методологии Data Science Lifecycle Process:
- Основная ветка - `main`.
- Для различных исследований/экспериментов создаются отдельные ветки, например: `research/example` или `experiment/example`.
- Далее ветки либо вливаются через `Merge request` в основную ветку, либо просто закрываются.

<p><small>Проект выполняется в рамках курса по MLOps на ODS.</small></p>

Сборка контейнера с проектом
------------

Для сборки образа необходимо выполнить команду:
- `docker build -t <image_name> .`

Для сборки образа в `dev` окружении (устанавливается `ruff` и `pre-commit`) необходимо выполнить команду:
- `docker build -t --build-arg ENV=dev <image_name> .`

Для запуска контейнера и перехода к консоль выполнить команду:
- `docker run -it <image_name> bash`
