# Hypothesis


Проведем тестирование функции, которая вычисляет
[WAPE](https://www.baeldung.com/cs/mape-vs-wape-vs-wmape).

1.  Текущая реализация

``` python
def wape(true: list, pred: list) -> float:
    """
    Calculate WAPE.

    Parameters
    ----------
    true : list
        True labels
    pred : list
        Prediction values

    Returns
    -------
    float
        WAPE metric
    """
    numerator = 0
    denominator = 0

    for i in range(len(true)):
        numerator += abs(true[i] - pred[i])
        denominator += abs(true[i])

    return numerator / denominator
```

1.  Функция должна принимать на вход два списка и возвращать посчитанное
    значение метрики WAPE.

2.  Реализуем тестовую функцию в hypothesis

``` python
import sys

sys.path.append("/Users/vkurichenko/Downloads/git/ods-mlops-2")

from src.utils.evaluate import wape
from hypothesis import given
from hypothesis.extra import numpy
from hypothesis.strategies import integers
import numpy as np

size = integers(min_value=0, max_value=10)


@given(
    true=numpy.arrays(dtype=np.float32, shape=size),
    pred=numpy.arrays(dtype=np.float32, shape=size),
)
def test_wape(true, pred):
    wape_metric = wape(true, pred)
    assert not np.nan(wape_metric)
```

1.  На каких значениях функция не выполняется

``` python
+-+---------------- 1 ----------------
| Traceback (most recent call last):
|   File "/Users/vkurichenko/Downloads/git/ods-mlops-2/src/test/test_evaluate.py", line 21, in test_wape
|     assert not np.nan(wape_metric)
| TypeError: 'float' object is not callable
| Falsifying example: test_wape(
|     true=array([0.], dtype=float32),
|     pred=array([0.], dtype=float32),  # or any other generated value
| )
+---------------- 2 ----------------
| Traceback (most recent call last):
|   File "/Users/vkurichenko/Downloads/git/ods-mlops-2/src/test/test_evaluate.py", line 20, in test_wape
|     wape_metric = wape(true, pred)
|   File "/Users/vkurichenko/Downloads/git/ods-mlops-2/src/utils/evaluate.py", line 28, in wape
|     numerator += abs(true[i] - pred[i])
| IndexError: index 0 is out of bounds for axis 0 with size 0
| Falsifying example: test_wape(
|     true=array([0.], dtype=float32),  # or any other generated value
|     pred=array([], dtype=float32),
| )
+---------------- 3 ----------------
| Traceback (most recent call last):
|   File "/Users/vkurichenko/Downloads/git/ods-mlops-2/src/test/test_evaluate.py", line 20, in test_wape
|     wape_metric = wape(true, pred)
|   File "/Users/vkurichenko/Downloads/git/ods-mlops-2/src/utils/evaluate.py", line 31, in wape
|     return numerator / denominator
| ZeroDivisionError: division by zero
| Falsifying example: test_wape(
|     # The test always failed when commented parts were varied together.
|     true=array([], dtype=float32),  # or any other generated value
|     pred=array([], dtype=float32),  # or any other generated value
| )
+------------------------------------
```

Таким образом, нам необходимо учесть в функции: 1. Обработку нулевых
значений 2. Обработку списков разной длины 3. Обработку пустых списков

1.  Изменяем функцию

``` python
def wape(true: np.ndarray, pred: np.ndarray) -> float:
    """
    Calculate WAPE.

    Parameters
    ----------
    true : list
        True labels
    pred : list
        Prediction values

    Returns
    -------
    float
        WAPE metric
    """
    if any(map(lambda x: len(x) == 0, [true, pred])):
        raise ValueError("One of the arrays is empty.")
    if len(true) != len(pred):
        raise ValueError("Arrays should be with the same size.")

    numerator = np.sum(np.abs(true - pred))
    denominator = np.sum(np.abs(true))

    if denominator == 0:
        return np.inf
    return numerator / denominator
```

5.1 Изменяем тесты hypothesis

``` python
import sys

sys.path.append("/Users/vkurichenko/Downloads/git/ods-mlops-2")

from src.utils.evaluate import wape
from hypothesis import given, assume, settings, HealthCheck
from hypothesis.extra import numpy
from hypothesis.strategies import integers, floats
import numpy as np

size = integers(min_value=0, max_value=10)
finite_f32 = floats(min_value=0, max_value=1e9, width=32, allow_infinity=False)


@settings(suppress_health_check=[HealthCheck.filter_too_much])
@given(
    true=numpy.arrays(dtype=np.float32, shape=size, elements=finite_f32),
    pred=numpy.arrays(dtype=np.float32, shape=size, elements=finite_f32),
)
def test_wape(true, pred):
    assume(len(true) > 0 and len(pred) > 0)
    assume(len(true) == len(pred))

    wape_metric = wape(true, pred)
    assert not np.isnan(wape_metric)
```

6.1 Немного меняем исходную функцию для oracle тестирования

``` python
def wape_old(true: list, pred: list) -> float:
    """
    Calculate WAPE.

    Parameters
    ----------
    true : list
        True labels
    pred : list
        Prediction values

    Returns
    -------
    float
        WAPE metric
    """
    numerator = 0
    denominator = 0

    for i in range(len(true)):
        numerator += abs(true[i] - pred[i])
        denominator += abs(true[i])

    if denominator == 0:
        return np.inf

    return np.float32(numerator / denominator)
```

6.2 Функция для oracle тестирования

``` python
@settings(suppress_health_check=[HealthCheck.filter_too_much])
@given(
    true=numpy.arrays(dtype=np.float32, shape=size, elements=finite_f32),
    pred=numpy.arrays(dtype=np.float32, shape=size, elements=finite_f32),
)
def test_wape_oracle(true, pred):
    assume(len(true) > 0 and len(pred) > 0)
    assume(len(true) == len(pred))

    wape_metric = wape(true, pred)
    wape_metric_old = wape_old(true, pred)
    assert np.isclose(wape_metric, wape_metric_old)
```
